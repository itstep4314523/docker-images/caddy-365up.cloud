FROM caddy:2.4.3-alpine

EXPOSE 80
EXPOSE 443

ADD ./Caddyfile /etc/caddy/Caddyfile